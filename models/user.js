'use strict';
const {
  Model
} = require('sequelize');
const { encrypt } = require('../helpers/encrypt.js');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Post)
      User.hasMany(models.Product)
    }
  };
  User.init({
    username: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: `Password required`
        },
        notEmpty: {
          args: true,
          msg: `Password cannot empty`
        },
        checkLength(value) {
          if(value.length < 6) {
            throw `Password at least 6 characters`
          }
        },
        hasNumber(value) {
          if(!/\d/.test(value)) {
            throw `Password must contain at least 1 number`;
          }
        }
      }
    }
  }, {
    hooks: {
      beforeCreate: (user) => {
        user.password = encrypt(user.password);
        return new Promise((resolve, reject) => {
          User.findOne({
            where: {
              username: user.username
            }
          })
            .then(result => {
              if(result) {
                const err = `Username Already Exists`;
                reject(err);
              } else {
                resolve(user);
              }
            })
            .catch(err => {
              reject(err);
            });
        });
      }
    },
    sequelize,
    modelName: 'User',
  });
  return User;
};