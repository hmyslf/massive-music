const { Post } = require('../models');

class PostController {
  static createPost (req, res, next) {
    let { title, content } = req.body;
    let UserId = req.UserId;
    Post.create({
      title,
      content,
      UserId,
      status: true
    })
      .then(data => {
        res.status(201).json({
          code: 201,
          message: 'success',
          data
        })
      })
      .catch(err => {
        return next(err);
      });
  }
  static getAllPost (req, res, next) {
    Post.findAll({ where: { status: true }})
      .then(data => {
        res.status(200).json({
          code: 200,
          message: 'success',
          data
        })
      })
      .catch(err => {
        return next(err);
      });
  }
  static getPostById (req, res, next) {
    let { id } = req.params;
    Post.findOne({
      where: {
        id,
        status: true
      }
    })
      .then(data => {
        if (data) {
          res.status(200).json({
            code: 200,
            message: 'success',
            data
          });
        } else {
          return next({
            code: 404,
            message: `Post Not Found`
          })
        }
      })
      .catch(err => {
        return next(err);
      })
  }
  static updatePost (req, res, next) {
    let { title, content } = req.body;
    let { id } = req.params;
    Post.update({
      title,
      content
    }, {
      where: {
        id
      },
      returning: true
    })
      .then(data => {
        if (data[0] > 0) {
          res.status(200).json({
            code: 200,
            message: 'success',
            data: data[1]
          })
        } else {
          return next({
            code: 404,
            name: "404NotFoundError",
            message: `Data Not Found`
          });
        }
      })
      .catch(err => {
        return next(err);
      })
  }
  static deletePost (req, res, next) {
    let { id } = req.params;
    Post.update({
      status: false
    }, {
      where: {
        id
      },
      returning: true
    })
    .then(data => {
      if (data[0] > 0) {
        res.status(200).json({
          code: 200,
          message: 'success',
          data: data[1]
        })
      } else {
        return next({
          code: 404,
          name: "404NotFoundError",
          message: `Data Not Found`
        });
      }
    })
    .catch(err => {
      return next(err);
    });
  }
}

module.exports = PostController;