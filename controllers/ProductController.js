const { Product } = require('../models');

class ProductController {
  static createProduct (req, res, next) {
    let { name, description } = req.body;
    let UserId = req.UserId;
    Product.create({
      name,
      description,
      UserId,
      status: true
    })
      .then(data => {
        res.status(201).json({
          code: 201,
          message: 'success',
          data
        })
      })
      .catch(err => {
        return next(err);
      });
  }
  static getAllProduct (req, res, next) {
    Product.findAll({ where: { status: true }})
      .then(data => {
        res.status(200).json({
          code: 200,
          message: 'success',
          data
        })
      })
      .catch(err => {
        return next(err);
      });
  }
  static getProductById (req, res, next) {
    let { id } = req.params;
    Product.findOne({
      where: {
        id,
        status: true
      }
    })
      .then(data => {
        if (data) {
          res.status(200).json({
            code: 200,
            message: 'success',
            data
          });
        } else {
          return next({
            code: 404,
            message: `Product Not Found`
          })
        }
      })
      .catch(err => {
        return next(err);
      })
  }
  static updateProduct (req, res, next) {
    let { name, description } = req.body;
    let { id } = req.params;
    Product.update({
      name,
      description
    }, {
      where: {
        id
      },
      returning: true
    })
      .then(data => {
        if (data[0] > 0) {
          res.status(200).json({
            code: 200,
            message: 'success',
            data: data[1]
          })
        } else {
          return next({
            code: 404,
            name: "404NotFoundError",
            message: `Data Not Found`
          });
        }
      })
      .catch(err => {
        return next(err);
      })
  }
  static deleteProduct (req, res, next) {
    let { id } = req.params;
    Product.update({
      status: false
    }, {
      where: {
        id
      },
      returning: true
    })
    .then(data => {
      if (data[0] > 0) {
        res.status(200).json({
          code: 200,
          message: 'success',
          data: data[1]
        })
      } else {
        return next({
          code: 404,
          name: "404NotFoundError",
          message: `Data Not Found`
        });
      }
    })
    .catch(err => {
      return next(err);
    });
  }
}

module.exports = ProductController;