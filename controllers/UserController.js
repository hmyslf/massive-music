const { User } = require('../models');
const { compare } = require('../helpers/encrypt.js');
const { userToken } = require('../helpers/jwt.js');

class UserController {
  static register (req, res, next) {
    let { username, password } = req.body;
    User.create({
      username,
      password
    })
      .then(data => {
        res.status(201).json({
          code: 201,
          message: 'Success Create User',
          data
        });
      })
      .catch(err => {
        return next(err);
      });
  }
  static login(req, res, next) {
    let { username, password } = req.body;
    User.findOne({
      where: {
        username
      }
    })
      .then(result => {
        if(result) {
          if(compare(password, result.password)) {
            let token = userToken({
              id: result.id,
              username: result.username
            });
            console.log(token, '===this is the token===');
            res.status(200).json({
              code: 200,
              message: 'success',
              data: { token }
            });
          } else {
            return next({
              code: 400,
              name: "Bad Request",
              message: `Password Salah`
            });
          }
        } else {
          return next({
            code: 400,
            name: "Bad Request",
            message: `Email/Password Salah`
          });
        }
      })
      .catch(err => {
        return next(err);
      })
  }
}

module.exports = UserController;