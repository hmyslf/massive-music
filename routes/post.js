const router = require('express').Router();
const PostController = require('../controllers/PostController.js');
const authentication = require('../middlewares/authentication.js');

router.use(authentication);
router.get('/', PostController.getAllPost);
router.post('/', PostController.createPost);
router.get('/:id', PostController.getPostById);
router.put('/:id', PostController.updatePost);
router.delete('/:id', PostController.deletePost);

module.exports = router;