const router = require('express').Router();
const userRouter = require('./user.js');
const postRouter = require('./post.js');
const productRouter = require('./product.js');

router.get('/', () => {
  console.log("Welcome to ToDo Apps");
});
router.use('/', userRouter);
router.use('/post', postRouter);
router.use('/product', productRouter);

module.exports = router;